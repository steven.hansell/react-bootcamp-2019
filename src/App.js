import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        React Tour of Heroes
      </header>
      <main role="main">
        <a href="https://angular.io/tutorial">Tour of Heroes Guide</a>
      </main>
    </div>
  );
}

export default App;
