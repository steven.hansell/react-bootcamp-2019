import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

if (!window.localStorage.getItem("heroes")) {
  window.localStorage.setItem("heroes", JSON.stringify(
    [
      {
        id: 1,
        name: "Wonder Woman"
      },
      {
        id: 2,
        name: "Hawkeye"
      },
      {
        id: 3,
        name: "Batman"
      },
      {
        id: 4,
        name: "Rogue"
      },
      {
        id: 5,
        name: "Thor"
      },
      {
        id: 6,
        name: "Gambit"
      }
    ]
  ));
}

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
